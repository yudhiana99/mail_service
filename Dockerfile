FROM golang:1.19-alpine

# Install curl 
RUN apk --no-cache add bash \
        curl \
        git \
        gcc \
        g++ \
        inotify-tools \
        ca-certificates

ENV APP_NAME hermes

WORKDIR /app/${APP_NAME}

# Copy go mod and sum files
ADD go.mod go.sum ./

ADD . /app/${APP_NAME}

RUN go mod download \
    && go install

EXPOSE 80

CMD [ "hermes" ] 