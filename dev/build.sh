#!/bin/bash

cp /app/hermes/env.example /app/hermes/.env

reflex -r '\.go$' -s -- sh -c 'go run /app/hermes/main.go'