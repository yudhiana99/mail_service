package main

import (
	"hermes/config"
	"hermes/helpers"
	"hermes/routers"

	"github.com/caarlos0/env/v6"
	_ "github.com/joho/godotenv/autoload"
)

func main() {

	// labels
	config.ShowLabel()

	cfg := config.Env{}
	if e := env.Parse(&cfg); e != nil {
		helpers.FatalPrintErr(e.Error())
	}

	if e := helpers.Validate("env", &cfg); e != nil {
		helpers.FatalPrintErr(e.Error())
	}

	// setup new router
	app := routers.NewRouter(&cfg)
	app.Setup()

}
