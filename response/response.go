package response

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func StatusOK(c *gin.Context) {
	c.JSONP(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "success",
	})
}

func Status(code int, msg interface{}, c *gin.Context) {
	c.JSONP(code, gin.H{
		"code":    code,
		"message": msg,
	})
}
