package helpers

import (
	"fmt"
	"os"
	"reflect"
	"strings"

	"github.com/TwinProduction/go-color"
	"github.com/go-playground/validator/v10"
)

func Validate(tagName string, s interface{}) error {
	validate := validator.New()
	validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get(tagName), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	if e := validate.Struct(s); e != nil {
		fields := []string{}
		for _, e := range e.(validator.ValidationErrors) {
			fields = append(fields, e.Field())
		}
		return fmt.Errorf("error : %v is required", strings.Join(fields, ","))
	}
	return nil
}

func PrintErr(msg string) {
	println(color.Ize(color.Red, fmt.Sprintf("[HERMES-DEBUG] %s", msg)))
}

func FatalPrintErr(msg string) {
	println(color.Ize(color.Red, fmt.Sprintf("[HERMES-DEBUG] %s", msg)))
	os.Exit(1)
}

func LogInfo(msg string) {
	println(color.Ize(color.Gray, fmt.Sprintf("[HERMES-DEBUG] %s", msg)))
}

func LogWarn(msg string) {
	println(color.Ize(color.Yellow, fmt.Sprintf("[HERMES-DEBUG] %s", msg)))
}

func IsEmptyString(s string) bool {
	s = strings.TrimSpace(s)
	return len(s) == 0 || s == ""
}
