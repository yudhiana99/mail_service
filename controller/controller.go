package controller

import (
	"encoding/base64"
	"fmt"
	"hermes/config"
	"hermes/helpers"
	"hermes/models"
	"hermes/response"
	"net/http"
	"net/smtp"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	goMail "gopkg.in/gomail.v2"
)

type mc struct {
	cfg *config.Env
}

func NewMailController(cfg *config.Env) *mc {
	return &mc{
		cfg: cfg,
	}
}

func (m *mc) SendMail(c *gin.Context) {
	var mailer models.Mail
	if err := c.BindJSON(&mailer); err != nil {
		response.Status(http.StatusUnprocessableEntity, "failed !", c)
		helpers.PrintErr(err.Error())
		return
	}

	if e := helpers.Validate("json", &mailer); e != nil {
		response.Status(http.StatusUnprocessableEntity, e.Error(), c)
		helpers.PrintErr(e.Error())
		return
	}

	errSend := m.Send(mailer.From, mailer.Subject, mailer.Message, mailer.To, mailer.Cc, "")
	if errSend != nil {
		response.Status(http.StatusUnprocessableEntity, errSend, c)
		helpers.PrintErr(errSend.Error())
		return
	}

	response.StatusOK(c)
}

func (m *mc) SendCustomEmail(addr, from, subject, body string, to, cc []string) error {
	r := strings.NewReplacer("\r\n", "", "\r", "", "\n", "", "%0a", "", "%0d", "")

	c, err := smtp.Dial(addr)
	if err != nil {
		return err
	}
	defer c.Close()
	if err = c.Mail(r.Replace(from)); err != nil {
		return err
	}
	for i := range to {
		to[i] = r.Replace(to[i])
		if err = c.Rcpt(to[i]); err != nil {
			return err
		}
	}

	w, err := c.Data()
	if err != nil {
		return err
	}

	content := buildMessage(from, subject, body, to, cc)
	fmt.Println(content)

	_, err = w.Write([]byte(content))
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	return c.Quit()
}

func buildMessage(from, subject, body string, to, cc []string) string {
	var sep string = "\n"
	var headers []string = []string{
		"From: " + from,
		"To: " + strings.Join(to, ","),
		"Cc: " + strings.Join(cc, ","),
		"Subject: " + subject,
		"Content-Type: text/html; charset=utf-8",
		"Content-Transfer-Encoding: base64",
	}

	var content []string = []string{
		sep,
		base64.StdEncoding.EncodeToString([]byte(body)),
	}

	return strings.Join(headers, sep) + strings.Join(content, sep)
}

func (m *mc) Send(from, subject, body string, to []string, cc []models.Cc, attachment string) error {

	mailer := goMail.NewMessage()
	mailer.SetHeader("From", from)
	mailer.SetHeader("To", to...)
	mailer.SetHeader("Subject", subject)
	mailer.SetBody("text/html", body)

	if helpers.IsEmptyString(from) {
		mailer.SetHeader("From", m.cfg.SenderName)
	}

	if attachment != "" {
		mailer.Attach(attachment)
	}

	if len(cc) > 0 {
		for _, c := range cc {
			mailer.SetAddressHeader("Cc", c.Email, c.Name)
		}
	}

	smtpPort, _ := strconv.Atoi(m.cfg.SmtpPort)
	dialer := goMail.NewDialer(
		m.cfg.SmtpHost,
		smtpPort,
		m.cfg.Email,
		m.cfg.Password,
	)

	helpers.LogInfo(fmt.Sprintf("sent mail to : %+v", to))
	return dialer.DialAndSend(mailer)
}
